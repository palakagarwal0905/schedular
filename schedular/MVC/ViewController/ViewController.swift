//
//  ViewController.swift
//  schedular
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Code-brew. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SVProgressHUD


var userModel :Instructor?

var fullDateValue : String?
var monthDateValue : String?
var tomorrowdate = Date()
var lastdate : String?

// Mark :ViewController Class//
class ViewController: UIViewController
{
    @IBOutlet weak var lblday7: UILabel!
    @IBOutlet weak var lblday6: UILabel!
    @IBOutlet weak var lblday5: UILabel!
    @IBOutlet weak var lblday4: UILabel!
    @IBOutlet weak var lblday3: UILabel!
    @IBOutlet weak var lblday2: UILabel!
    @IBOutlet weak var lblday1: UILabel!
    
    
    @IBOutlet weak var lbldate: UILabel!
    @IBOutlet weak var CollectionViewoutlet: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let fullDate = DateFormatter()
        let monthDate = DateFormatter()
        fullDate.dateFormat = "yyyy-MM-dd"
        monthDate.dateFormat = "MMM-dd"
        monthDateValue = monthDate.string(from: Date())
        fullDateValue = fullDate.string(from: Date())
        let todayDate = Date()
        let tomorrowDate = DateFormatter()
        tomorrowDate.dateFormat = "yyyy-MM-dd"
        tomorrowdate = tomorrowDate.calendar.date(byAdding: .day, value: 6,to: todayDate)!
        lastdate = tomorrowDate.string(from: tomorrowdate)
        //lbldate.text = "Today, " + monthDateValue!
        lbldate.text = monthDateValue!
        let weekDay = Calendar.current.component(.weekday, from: Date())
        Alamofire.request("http://34.195.206.185/api/instructor-home")
        SVProgressHUD.show()
        SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
            fetchData()
    }
    
    
    //function to get days of a week//
    func getDayOfWeekString(today:String)->String? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            let myComponents = myCalendar.components(.weekday, from: todayDate)
            let weekDay = myComponents.weekday
            switch (weekDay!) {
            case 1:
                return "Sun"
            case 2:
                return "Mon"
            case 3:
                return "Tues"
            case 4:
                return "Wed"
            case 5:
                return "Thu"
            case 6:
                return "Fri"
            case 7:
                return "Sat"
            default:
                return "Nil"
            }
        }
        return nil
    }
    func labelbold(Daylabel:UILabel)
    {
        var lblarrday = [ lblday1 , lblday2 ,lblday3 ,lblday4 ,lblday5 ,lblday6, lblday7]
           for index in 0..<lblarrday.count
            {
            if(Daylabel == lblarrday[index])
            {
               Daylabel.font = UIFont.systemFont(ofSize: 17, weight: 4)
        }
        else
            {
                lblarrday[index]?.font = UIFont.systemFont(ofSize:14, weight:0)
         }
        }
    }
    
    //function for fetching data
    func fetchData() {
        
        let param:[String:Any] = ["access_token" : "ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8" , "page_no": "1" ,"page_size" : "7" ,"date_selected" :"2017-02-24"]
        
        ApiHandler.fetchData(urlStr: "instructor-home", parameters: param) {
            (jsonData) in
            
            userModel = Mapper<Instructor>().map(JSONObject: jsonData)
            self.CollectionViewoutlet.reloadData()
            print(userModel?.msg ?? "")
            print(jsonData)
            
            self.lbldate.text = userModel?.idata?[0].date ?? ""
            
            self.lblday1.text = self.getDayOfWeekString(today:(userModel?.idata?[0].date1!)!)
            self.lblday2.text = self.getDayOfWeekString(today:(userModel?.idata?[1].date1!)!)
            self.lblday3.text = self.getDayOfWeekString(today:(userModel?.idata?[2].date1!)!)
            self.lblday4.text = self.getDayOfWeekString(today:(userModel?.idata?[3].date1!)!)
            self.lblday5.text = self.getDayOfWeekString(today:(userModel?.idata?[4].date1!)!)
            self.lblday6.text = self.getDayOfWeekString(today:(userModel?.idata?[5].date1!)!)
            self.lblday7.text = self.getDayOfWeekString(today:(userModel?.idata?[6].date1!)!)
           /* print(modeldetails.idata?[0].day ?? "")
            self.lblday1.text = userModel.idata?[0].day ?? ""
            self.lblday2.text = userModel.idata?[1].day ?? ""
            self.lblday3.text = userModel.idata?[2].day ?? ""
            self.lblday4.text = userModel.idata?[3].day ?? ""
            self.lblday5.text = userModel.idata?[4].day ?? ""
            self.lblday6.text = userModel.idata?[5].day ?? ""
            self.lblday7.text = userModel.idata?[6].day ?? ""*/
            
        }
    }
    
    
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: - ColletionView Datasources/Delegates
extension ViewController : UICollectionViewDataSource , UICollectionViewDelegate,UIScrollViewDelegate, UICollectionViewDelegateFlowLayout
{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section:Int) -> Int
    {
        return userModel?.idata?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt IndexPath: IndexPath)  -> UICollectionViewCell
    {  SVProgressHUD.dismiss()
        let collectioncell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for : IndexPath) as! CollectionViewCell
      collectioncell.userdetails = (userModel?.idata?[IndexPath.row].details)!
        return collectioncell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let cellWidth = self.CollectionViewoutlet.frame.width
    let cellHeight = self.CollectionViewoutlet.frame.height
    return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        var visibleRect = CGRect()
        visibleRect.origin = CollectionViewoutlet.contentOffset
        visibleRect.size = CollectionViewoutlet.bounds.size
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        let visibleIndexPath : IndexPath = CollectionViewoutlet.indexPathForItem(at: visiblePoint)!
        let lastElement = (userModel?.idata?.count)! - 1
        if visibleIndexPath.row == lastElement
        {
            Alamofire.request("http://34.195.206.185/api/instructor-home")
            let param: [String:Any] = ["access_token": "ymaANbhfJT4ARby5IbK2u0hUJQ9T7dk8", "page_no": "1", "page_size": "7","last_date": lastdate ?? "", "date_selected": fullDateValue!]
            SVProgressHUD.show()
            SVProgressHUD.setDefaultAnimationType(SVProgressHUDAnimationType.native)
            SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
            ApiHandler.fetchData(urlStr: "instructor-home", parameters: param)
            {
                (jsonData) in
                let userModelAppend = Mapper<Instructor>().map(JSONObject: jsonData)
                for temp in 0..<7
                {
                    userModel?.idata?.append((userModelAppend?.idata?[temp])!)
                }
                SVProgressHUD.dismiss()
                self.CollectionViewoutlet.reloadData()
            }
        }
    }    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
   {
    if(indexPath.item % 7 == 0)
   {
       self.labelbold(Daylabel: lblday1)
    }
        else if (indexPath.item % 7 == 1)
    {
        self.labelbold(Daylabel: lblday2)
   }
    else if (indexPath.item % 7 == 2)
   {
       self.labelbold(Daylabel: lblday3)
   }
   else if (indexPath.item % 7 == 3)
   {
      self.labelbold(Daylabel: lblday4)
    }
    else if (indexPath.item % 7 == 4)
    {
        self.labelbold(Daylabel: lblday5)
    }
    else if (indexPath.item % 7 == 5)
    {
       self.labelbold(Daylabel: lblday6)
   }
    else if (indexPath.item % 7 == 6)
   {
        self.labelbold(Daylabel: lblday7)
    }
  }
}
