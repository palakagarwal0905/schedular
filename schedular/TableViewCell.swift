//
//  TableViewCell.swift
//  schedular
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Code-brew. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire


class TableViewCell: UITableViewCell {
    
   
    
    @IBOutlet weak var viewoutlet: UIView!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblHome: UILabel!
    @IBOutlet weak var lblEnrolledNumber: UILabel!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblHours: UILabel!
  
    
    

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    }
    
    

