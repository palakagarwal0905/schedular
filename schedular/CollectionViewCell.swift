//
//  CollectionViewCell.swift
//  schedular
//
//  Created by Sierra 4 on 23/02/17.
//  Copyright © 2017 Code-brew. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell
{
    var userdetails : [Details]?
    var subject : [Subjects]?
    @IBOutlet weak var TableViewOutlet: UITableView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        TableViewOutlet.delegate = self
        TableViewOutlet.dataSource = self
    }
}


//........ EXTENSION : TableView DataSource/Delegates.......//
extension CollectionViewCell : UITableViewDelegate ,UITableViewDataSource
{
    
//....Mark: No. of rows in section....//
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return userdetails?.count ?? 0
    }
    
//Mark.....Cell for row At.....//
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       let tablecell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as? TableViewCell
        
        tablecell?.lblAge.text = (userdetails?[indexPath.row].age_group)!
        tablecell?.lblAddress.text = (userdetails?[indexPath.row].day_location)!
        tablecell?.lblAmount.text = (userdetails?[indexPath.row].charge_student)!
        tablecell?.lblTime.text = (userdetails?[indexPath.row].start_time)!
        tablecell?.lblHours.text = (userdetails?[indexPath.row].time_duration)!
        tablecell?.lblEnrolledNumber.text = String(describing: (userdetails?[indexPath.row].enrolled) ?? 0)
        
//Mark:....for creating view border and shadow.....//
        tablecell?.viewoutlet.layer.shadowColor = UIColor.gray.cgColor
        tablecell?.viewoutlet.layer.shadowOpacity = 1
        tablecell?.viewoutlet.layer.shadowOffset = CGSize(width: 1 , height: 1)
        tablecell?.viewoutlet.layer.shadowRadius = 2
        
        for subject in (userdetails?[indexPath.row].subjects)!
        {
            tablecell?.lblSubject.text = subject.subject_name ?? ""
        }
        return tablecell!
    }
    
    
   }
